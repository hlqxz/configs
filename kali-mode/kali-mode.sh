#!/usr/bin/env bash

light_mode() {
	/usr/bin/xfconf-query -c xsettings -p /Net/IconThemeName -s Flat-Remix-Blue-Light;
	/usr/bin/xfconf-query -c xsettings -p /Net/ThemeName -s Kali-Light;
	/usr/bin/xfconf-query -c xfwm4 -p /general/theme -s Kali-Light;
	/usr/bin/gsettings set org.xfce.mousepad.preferences.view color-scheme Kali-Light;
	echo "Changed to Light Mode";
}

dark_mode() {
	/usr/bin/xfconf-query -c xsettings -p /Net/IconThemeName -s Flat-Remix-Blue-Dark;
	/usr/bin/xfconf-query -c xsettings -p /Net/ThemeName -s Kali-Dark;
	/usr/bin/xfconf-query -c xfwm4 -p /general/theme -s Kali-Dark;
	/usr/bin/gsettings set org.xfce.mousepad.preferences.view color-scheme Kali-Dark;
	echo "Changed to Dark Mode";
}

MODE=$1;
if [ "$MODE" = "light" ]; then
	light_mode;
elif [ "$MODE" = "dark" ]; then
	dark_mode;
else
	:
fi
